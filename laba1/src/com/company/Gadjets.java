package com.company;

import java.util.UUID;

public abstract class Gadjets implements ICrudAction {

    @Override
    public void create() {
    }

    @Override
    public void delete() {
    }

    @Override
    public void read() {
    }

    @Override
    public void update(){

    }


    protected UUID id;
    String name;
    int price;
    int counter;
    String firm;
    String model;
    String os;
}
