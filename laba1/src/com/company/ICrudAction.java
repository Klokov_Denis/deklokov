package com.company;

public  interface ICrudAction {
    void create();

    void update();

    void read();

    void delete();


}
